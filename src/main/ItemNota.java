package main;

public class ItemNota {
	private Item item;
	private double quantidade;
	
	public ItemNota(Item item, double quantidade) throws IllegalArgumentException {
		setItem(item);
		setQuantidade(quantidade);
	}
	
	public double getQuantidade() {
		return quantidade;
	}
	
	public String getQuantidadeString() {
		return String.format(item.getTipo().getVenda().getQuantidadeFormat(), quantidade);
	}
	
	public void setQuantidade(double quantidade) {
		this.quantidade = (quantidade > 0) ? quantidade : 0;
	}
	
	public Item getItem() {
		return item;
	}
	
	public void setItem(Item item) throws IllegalArgumentException {
		if (item == null)
			throw new IllegalArgumentException();
		this.item = item;
	}
	
	public String getPrecoStringExt() {
		return item.getPrecoStringExt();
	}
}
