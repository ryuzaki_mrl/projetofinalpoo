package control;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javax.swing.JOptionPane;
import model.LoginModel;
import view.Main;

public class LoginController {
	@FXML private TextField login;
	@FXML private TextField senha;
	private LoginModel model;
	
	@FXML protected void autenticar(ActionEvent event) {
		model = new LoginModel();
		try {
			model.autenticar(login.getText(), senha.getText());
			Main.telaPrincipal();
		} catch(Exception e) {
			JOptionPane.showMessageDialog(null, "Login e/ou senha incorreto(s).");
		}
	}
}
