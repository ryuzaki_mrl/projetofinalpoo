package main;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class NotaCompras {
	private ObservableList<ItemNota> itens;
	private DoubleProperty total;
	
	public NotaCompras() {
		itens = FXCollections.observableArrayList();
		total = new SimpleDoubleProperty();
	}
	
	public ObservableList<ItemNota> getItens() {
		return itens;
	}
	
	public DoubleProperty totalProperty() {
		return total;
	}
	
	public void setTotal(double total) {
		this.total.set((total > 0) ? total : 0);
	}
	
	public double getTotal() {
		return total.get();
	}
	
	public boolean add(Item item, double quantidade) {
		if (item == null || quantidade < 0 || item.getEstoque() < quantidade)
			return false;
		for (ItemNota i : itens) {
			if (i.getItem().equals(item)) {
				if (i.getQuantidade() + quantidade <= item.getEstoque()) {
					i.setQuantidade(i.getQuantidade() + quantidade);
					setTotal(total.get() + (item.getPreco() * quantidade));
					return true;
				} else {
					return false;
				}
			}
		}
		itens.add(new ItemNota(item, quantidade));
		setTotal(total.get() + (item.getPreco() * quantidade));
		return true;
	}
	
	public boolean remover(ItemNota item, double quantidade) {
		if (item == null || quantidade < 0)
			return false;
		if (quantidade >= item.getQuantidade()) {
			itens.remove(item);
			setTotal(total.get() - (item.getItem().getPreco() * item.getQuantidade()));
		} else {
			item.setQuantidade(item.getQuantidade() - quantidade);
			setTotal(total.get() - (item.getItem().getPreco() * quantidade));
		}
		return true;
	}
	
	public void processar() {
		for (ItemNota i : itens) {
			double estoque = i.getItem().getEstoque();
			i.getItem().setEstoque(estoque - i.getQuantidade());
		}
	}
	
	@Override
	public String toString() {
		String out = (
			"----------------------------------------------------------\n"+
			"MERCAD�O DO GABRIEL\n"+
			"----------------------------------------------------------\n"
		);
		for (ItemNota i : itens) {
			String abbr = i.getItem().getTipo().getVenda().getAbreviacao();
			out += i.getItem().toString() + " R$ " + i.getItem().getPrecoString() + " x" + i.getQuantidadeString() + " " + abbr + "\n";
		}
		out += (
			"----------------------------------------------------------\n"+
			"Total: " + String.format("R$ %.02f", total.get()) + "\n"+
			"----------------------------------------------------------\n"
		);
		return out;
	}
}
