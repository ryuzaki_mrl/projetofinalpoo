package model;

import main.Database;
import main.Item;
import main.Tipo;

public class CadastrarItemModel {
	public void adicionar(String marca, String codigo, double estoque, double preco, Tipo tipo) throws Exception {
		Item item = new Item();
		item.setCodigo(codigo);
		item.setMarca(marca);
		item.setEstoque(estoque);
		item.setPreco(preco);
		item.setTipo(tipo);
		Database.getInstance().getItens().add(item);
	}

	public void atualizar(Item item, String marca, String codigo, double estoque, double preco, Tipo tipo) throws Exception {
		item.setCodigo(codigo);
		item.setMarca(marca);
		item.setEstoque(estoque);
		item.setPreco(preco);
		item.setTipo(tipo);
		Database.getInstance().exportar();
	}

	public void remover(Item item) {
		Database.getInstance().getItens().remove(item);
	}
}
