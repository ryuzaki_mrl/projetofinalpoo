package main;

public class VendaQuilo extends StrategyVenda {
	private static final long serialVersionUID = 9030234427206752025L;

	@Override
	public String getAbreviacao() {
		return "kg";
	}
	
	@Override
	public String getQuantidadeFormat() {
		return "%.03f";
	}
	
	@Override
	public String toString() {
		return "Por quilo";
	}
}
