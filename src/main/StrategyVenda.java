package main;

import java.util.ArrayList;

public abstract class StrategyVenda implements java.io.Serializable {
	private static final long serialVersionUID = -4435748535665430601L;
	
	public abstract String getAbreviacao();
	public abstract String getQuantidadeFormat();
	
	public static ArrayList<StrategyVenda> getList() {
		ArrayList<StrategyVenda> lista = new ArrayList<StrategyVenda>();
		lista.add(new VendaQuilo());
		lista.add(new VendaUnidade());
		lista.add(new VendaLitro());
		return lista;
	}
}
