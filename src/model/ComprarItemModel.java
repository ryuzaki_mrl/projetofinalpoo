package model;

import main.Database;
import main.Item;

public class ComprarItemModel {
	public void comprar(Item item, double quantidade) throws IllegalArgumentException {
		if (item == null || quantidade <= 0)
			throw new IllegalArgumentException();
		item.setEstoque(item.getEstoque() + quantidade);
		Database.getInstance().exportar();
	}
}
