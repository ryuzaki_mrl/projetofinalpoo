package control;

import javax.swing.JOptionPane;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import view.Main;
import main.Database;
import main.Item;
import main.Tipo;
import model.CadastrarItemModel;

public class CadastrarItemController {
	@FXML private TextField textMarca;
	@FXML private TextField textCodigo;
	@FXML private TextField textEstoque;
	@FXML private TextField textPreco;
	@FXML private TextField searchItem;
	@FXML private ChoiceBox<Tipo> choiceTipo;
	@FXML private TableView<Item> tableItens;
	@FXML private TableColumn<Item,String> columnCodigo;
	@FXML private TableColumn<Item,String> columnMarca;
	@FXML private TableColumn<Item,Tipo> columnTipo;
	private CadastrarItemModel model;

	@FXML public void initialize() {
		tableItens.setItems(ItemManager.setItemSearchFilter(searchItem));
		columnCodigo.setCellValueFactory(new PropertyValueFactory<Item,String>("codigo"));
		columnMarca.setCellValueFactory(new PropertyValueFactory<Item,String>("marca"));
		columnTipo.setCellValueFactory(new PropertyValueFactory<Item,Tipo>("tipo"));
		tableItens.getSelectionModel().selectedItemProperty().addListener((observable, olditem, newitem) -> {
			if (newitem==null) return;
	    	textMarca.setText(newitem.getMarca());
			textCodigo.setText(newitem.getCodigo());
			textEstoque.setText(newitem.getEstoqueString());
			textPreco.setText(newitem.getPrecoString());
			choiceTipo.setValue(newitem.getTipo());
		});

		choiceTipo.setItems(Database.getInstance().getTipos());
		model = new CadastrarItemModel();
	}

	@FXML protected void adicionar() {
		try {
			model.adicionar(textMarca.getText(), textCodigo.getText(), Double.parseDouble(textEstoque.getText()), Double.parseDouble(textPreco.getText()), choiceTipo.getValue());
			limpar();
		} catch(IllegalArgumentException e) {
			JOptionPane.showMessageDialog(null, "Entrada inv�lida.");
		} catch(Exception e) {
			JOptionPane.showMessageDialog(null, "Erro desconhecido.");
		}
	}

	@FXML protected void atualizar() {
		Item item = tableItens.getSelectionModel().getSelectedItem();
		try {
			model.atualizar(item, textMarca.getText(), textCodigo.getText(), Double.parseDouble(textEstoque.getText()), Double.parseDouble(textPreco.getText()), choiceTipo.getValue());
			tableItens.refresh();
		} catch(NullPointerException e) {
			JOptionPane.showMessageDialog(null, "Nenhum item selecionado.");
		} catch(IllegalArgumentException e) {
			JOptionPane.showMessageDialog(null, "Entrada inv�lida.");
		} catch(Exception e) {
			JOptionPane.showMessageDialog(null, "Erro desconhecido.");
		}
	}

	@FXML protected void remover() {
		model.remover(tableItens.getSelectionModel().getSelectedItem());
	}

	@FXML protected void voltar() {
		Main.telaPrincipal();
	}

	@FXML protected void novoTipo() {
		Main.telaCadastrarTipo();
	}

	protected void limpar() {
		textMarca.clear();
		textCodigo.clear();
		textEstoque.clear();
		textPreco.clear();
		choiceTipo.setValue(null);
	}
}
