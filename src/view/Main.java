package view;

import java.util.Locale;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import main.Database;

public class Main extends Application {
	private static Stage stage;

	private static void novaTela(String fxml) {
		try {
			Parent root = FXMLLoader.load(Main.class.getResource(fxml));
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public static void setStage(Stage stage) {
		Main.stage = stage;
	}

	public static void telaLogin() {
		novaTela("login.fxml");
	}
	
	public static void telaPrincipal() {
		novaTela("principal.fxml");
	}
	
	public static void telaCadastrarItem() {
		novaTela("cadastrar.fxml");
	}
	
	public static void telaCadastrarTipo() {
		novaTela("novotipo.fxml");
	}
	
	public static void telaComprar() {
		novaTela("comprar.fxml");
	}
	
	public static void telaVender() {
		novaTela("vender.fxml");
	}

	public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		launch(args);
		Database.getInstance().exportar();
	}

	@Override
	public void start(Stage stage) {
		stage.setTitle("Mercad�o do Gabriel");
		stage.setResizable(false);
		setStage(stage);
		telaLogin();
	}
}
