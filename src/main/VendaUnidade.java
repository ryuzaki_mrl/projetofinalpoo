package main;

public class VendaUnidade extends StrategyVenda {
	private static final long serialVersionUID = -1199716051122317623L;
	
	@Override
	public String getAbreviacao() {
		return "und";
	}
	
	@Override
	public String getQuantidadeFormat() {
		return "%.00f";
	}
	
	@Override
	public String toString() {
		return "Por unidade";
	}
}
