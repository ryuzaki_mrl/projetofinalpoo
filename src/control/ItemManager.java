package control;

import javafx.scene.control.TextField;
import javafx.collections.transformation.FilteredList;
import main.Database;
import main.Item;
import main.ItemNota;
import main.NotaCompras;

public class ItemManager {
	public static FilteredList<Item> setItemSearchFilter(TextField searchItem) {
		FilteredList<Item> filteredData = new FilteredList<Item>(Database.getInstance().getItens());
		searchItem.textProperty().addListener((observable, oldvalue, newvalue) -> {
			filteredData.setPredicate(pItem -> {
				if (newvalue == null || newvalue.isEmpty())
					return true;
				String value = newvalue.toLowerCase();
				if (pItem.getCodigo().toLowerCase().contains(value) || pItem.getMarca().toLowerCase().contains(value))
					return true;
				return false;
			});
		});
		return filteredData;
	}
	
	public static FilteredList<ItemNota> setItemNotaSearchFilter(TextField searchItem, NotaCompras nota) {
		FilteredList<ItemNota> filteredData = new FilteredList<ItemNota>(nota.getItens());
		searchItem.textProperty().addListener((observable, oldvalue, newvalue) -> {
			filteredData.setPredicate(pItem -> {
				if (newvalue == null || newvalue.isEmpty())
					return true;
				String value = newvalue.toLowerCase();
				if (pItem.getItem().getCodigo().toLowerCase().contains(value) || pItem.getItem().getMarca().toLowerCase().contains(value))
					return true;
				return false;
			});
		});
		return filteredData;
	}
}
