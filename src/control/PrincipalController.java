package control;

import javafx.fxml.FXML;
import view.Main;

public class PrincipalController {
	@FXML protected void sair() {
		Main.telaLogin();
	}

	@FXML protected void cadastrar() {
		Main.telaCadastrarItem();
	}

	@FXML protected void comprar() {
		Main.telaComprar();
	}

	@FXML protected void vender() {
		Main.telaVender();
	}
}
