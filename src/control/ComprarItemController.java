package control;

import javax.swing.JOptionPane;
import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import main.Item;
import main.Tipo;
import model.ComprarItemModel;
import view.Main;

public class ComprarItemController {
	@FXML private TextField textItem;
	@FXML private TextField textQuantidade;
	@FXML private TextField textEstoque;
	@FXML private TextField textPreco;
	@FXML private TextField textTotal;
	@FXML private TextField searchItem;
	@FXML private TableView<Item> tableItens;
	@FXML private TableColumn<Item,String> columnCodigo;
	@FXML private TableColumn<Item,String> columnMarca;
	@FXML private TableColumn<Item,Tipo> columnTipo;
	@FXML private TableColumn<Item,String> columnPreco;
	private ComprarItemModel model;
	
	@FXML public void initialize() {
		textTotal.textProperty().bind(Bindings.createStringBinding(() -> {
			try {
				Item item = tableItens.getSelectionModel().selectedItemProperty().get();
				double quantidade = Double.parseDouble(textQuantidade.textProperty().get());
				return String.format("R$ %.02f", quantidade * item.getPrecoCompra());
			} catch(Exception e) {
				return "R$ 0.00";
			}
		}, tableItens.getSelectionModel().selectedItemProperty(), textQuantidade.textProperty()));

		tableItens.setItems(ItemManager.setItemSearchFilter(searchItem));
		columnCodigo.setCellValueFactory(new PropertyValueFactory<Item,String>("codigo"));
		columnMarca.setCellValueFactory(new PropertyValueFactory<Item,String>("marca"));
		columnTipo.setCellValueFactory(new PropertyValueFactory<Item,Tipo>("tipo"));
		columnPreco.setCellValueFactory(new PropertyValueFactory<Item,String>("precoCompraString"));
		tableItens.getSelectionModel().selectedItemProperty().addListener((observable, olditem, newitem) -> {
			if (newitem==null) return;
			textItem.setText(newitem.toString());
			textEstoque.setText(newitem.getEstoqueString());
			textPreco.setText(newitem.getPrecoCompraString());
		});
		
		model = new ComprarItemModel();
	}
	
	@FXML protected void comprar() {
		try {
			Item item = tableItens.getSelectionModel().getSelectedItem();
			model.comprar(item, Double.parseDouble(textQuantidade.getText()));
			textEstoque.setText(item.getEstoqueString());
		} catch(IllegalArgumentException e) {
			JOptionPane.showMessageDialog(null, "Entrada inv�lida.");
		}
	}
	
	@FXML protected void voltar() {
		Main.telaPrincipal();
	}
}
