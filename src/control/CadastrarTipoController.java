package control;

import javax.swing.JOptionPane;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import view.Main;
import main.Database;
import main.Item;
import main.StrategyVenda;
import main.Tipo;
import model.CadastrarTipoModel;

public class CadastrarTipoController {
	@FXML private TextField textNome;
	@FXML private TextField textCodigo;
	@FXML private ChoiceBox<StrategyVenda> choiceVenda;
	@FXML private TableView<Tipo> tableTipos;
	@FXML private TableColumn<Item,String> columnCodigo;
	@FXML private TableColumn<Item,String> columnNome;
	@FXML private TableColumn<Item,StrategyVenda> columnVenda;
	private CadastrarTipoModel model;

	@FXML public void initialize() {
		tableTipos.setItems(Database.getInstance().getTipos());
		columnCodigo.setCellValueFactory(new PropertyValueFactory<Item,String>("codigo"));
		columnNome.setCellValueFactory(new PropertyValueFactory<Item,String>("nome"));
		columnVenda.setCellValueFactory(new PropertyValueFactory<Item,StrategyVenda>("venda"));
		tableTipos.getSelectionModel().selectedItemProperty().addListener((observable, oldtipo, newtipo) -> {
	    	textNome.setText(newtipo.getNome());
			textCodigo.setText(newtipo.getCodigo());
			choiceVenda.setValue(newtipo.getVenda());
		});

		choiceVenda.setItems(FXCollections.observableArrayList(Database.getInstance().getVendas()));
		model = new CadastrarTipoModel();
	}

	@FXML protected void adicionar() {
		try {
			model.adicionar(textNome.getText(), textCodigo.getText(), choiceVenda.getValue());
			limpar();
		} catch(IllegalArgumentException e) {
			JOptionPane.showMessageDialog(null, "Entrada inv�lida.");
		} catch(Exception e) {
			JOptionPane.showMessageDialog(null, "Erro desconhecido.");
		}
	}

	@FXML protected void atualizar() {
		Tipo tipo = tableTipos.getSelectionModel().getSelectedItem();
		try {
			model.atualizar(tipo, textNome.getText(), textCodigo.getText(), choiceVenda.getValue());
			tableTipos.refresh();
		} catch(NullPointerException e) {
			JOptionPane.showMessageDialog(null, "Nenhum tipo selecionado.");
		} catch(IllegalArgumentException e) {
			JOptionPane.showMessageDialog(null, "Entrada inv�lida.");
		} catch(Exception e) {
			JOptionPane.showMessageDialog(null, "Erro desconhecido.");
		}
	}

	@FXML protected void remover() {
		model.remover(tableTipos.getSelectionModel().getSelectedItem());
	}

	@FXML protected void voltar() {
		Main.telaCadastrarItem();
	}

	protected void limpar() {
		textNome.setText("");
		textCodigo.setText("");
		choiceVenda.setValue(null);
	}
}
