package main;

public class VendaLitro extends StrategyVenda {
	private static final long serialVersionUID = 8284563662008522757L;

	@Override
	public String getAbreviacao() {
		return "l";
	}
	
	@Override
	public String getQuantidadeFormat() {
		return "%.03f";
	}
	
	@Override
	public String toString() {
		return "Por litro";
	}
}
