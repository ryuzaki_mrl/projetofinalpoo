package main;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;

public final class Database {
	private static Database instance;
	private ObservableList<Item> itens;
	private ObservableList<Tipo> tipos;
	private ArrayList<StrategyVenda> vendas;
	private double saldoLoja;
	
	@SuppressWarnings("unchecked")
	private Database() {
		try {
            FileInputStream fs = new FileInputStream("database.bin");
            ObjectInputStream os = new ObjectInputStream(fs);
            DataInputStream ds = new DataInputStream(fs);
            itens = FXCollections.observableArrayList((ArrayList<Item>)os.readObject());
            tipos = FXCollections.observableArrayList((ArrayList<Tipo>)os.readObject());
            vendas = (ArrayList<StrategyVenda>)os.readObject();
            saldoLoja = ds.readDouble();
            ds.close();
            os.close();
            fs.close();
        } catch(Exception e) {
        	itens = FXCollections.observableArrayList();
        	tipos = FXCollections.observableArrayList();
        	vendas = StrategyVenda.getList();
        	saldoLoja = 1000.00;
        }
		ListChangeListener<Serializable> listener = new ListChangeListener<Serializable>() {
			@Override
			public void onChanged(Change<? extends Serializable> c) {
				exportar();
			}
		};
		itens.addListener(listener);
		tipos.addListener(listener);
	}

	public static synchronized Database getInstance() {
		if (instance == null) {
			instance = new Database();
		}
		return instance;
	}

	public ObservableList<Item> getItens() {
		return itens;
	}
	
	public ObservableList<Tipo> getTipos() {
		return tipos;
	}
	
	public ArrayList<StrategyVenda> getVendas() {
		return vendas;
	}

	public double getSaldoLoja() {
		return saldoLoja;
	}

	public void setSaldoLoja(double saldo) throws IllegalArgumentException {
		if (saldo < 0)
			throw new IllegalArgumentException();
		saldoLoja = saldo;
	}

	public void exportar() {
		try {
			FileOutputStream fs = new FileOutputStream("database.bin");
			ObjectOutputStream os = new ObjectOutputStream(fs);
			DataOutputStream ds = new DataOutputStream(fs);
			os.writeObject(new ArrayList<Item>(itens));
			os.writeObject(new ArrayList<Tipo>(tipos));
			os.writeObject(vendas);
			ds.writeDouble(saldoLoja);
			ds.close();
			os.close();
			fs.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
}
