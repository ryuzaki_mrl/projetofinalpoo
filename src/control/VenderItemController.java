package control;

import javax.swing.JOptionPane;

import javafx.beans.binding.Bindings;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import main.Item;
import main.ItemNota;
import main.NotaCompras;
import main.Tipo;
import model.VenderItemModel;
import view.Main;

public class VenderItemController {
	@FXML private TextField textItem;
	@FXML private TextField textQuantidadeAdd;
	@FXML private TextField textQuantidadeRem;
	@FXML private TextField textEstoque;
	@FXML private TextField textPreco;
	@FXML private TextField textTotal;
	@FXML private TextField searchItem;
	@FXML private TextField searchCarrinho;
	@FXML private TextArea textNota;
	@FXML private TableView<Item> tableItens;
	@FXML private TableView<ItemNota> tableCarrinho;
	@FXML private TableColumn<Item,String> columnCodigoA;
	@FXML private TableColumn<Item,String> columnMarca;
	@FXML private TableColumn<Item,Tipo> columnTipo;
	@FXML private TableColumn<Item,String> columnPrecoA;
	@FXML private TableColumn<Item,String> columnEstoque;
	@FXML private TableColumn<ItemNota,Item> columnItem;
	@FXML private TableColumn<ItemNota,String> columnPrecoB;
	@FXML private TableColumn<ItemNota,String> columnQuantidade;
	private VenderItemModel model;
	private NotaCompras nota;
	
	@FXML public void initialize() {
		nota = new NotaCompras();
		
		textTotal.textProperty().bind(Bindings.createStringBinding(() -> {
			try {
				return String.format("R$ %.02f", nota.totalProperty().get());
			} catch(Exception e) {
				return "R$ 0.00";
			}
		}, nota.totalProperty()));

		tableItens.setItems(ItemManager.setItemSearchFilter(searchItem));
		columnCodigoA.setCellValueFactory(new PropertyValueFactory<Item,String>("codigo"));
		columnMarca.setCellValueFactory(new PropertyValueFactory<Item,String>("marca"));
		columnTipo.setCellValueFactory(new PropertyValueFactory<Item,Tipo>("tipo"));
		columnPrecoA.setCellValueFactory(new PropertyValueFactory<Item,String>("precoStringExt"));
		columnEstoque.setCellValueFactory(new PropertyValueFactory<Item,String>("estoqueString"));
		
		tableCarrinho.setItems(ItemManager.setItemNotaSearchFilter(searchCarrinho, nota));
		columnItem.setCellValueFactory(new PropertyValueFactory<ItemNota,Item>("item"));
		columnPrecoB.setCellValueFactory(new PropertyValueFactory<ItemNota,String>("precoStringExt"));
		columnQuantidade.setCellValueFactory(new PropertyValueFactory<ItemNota,String>("quantidadeString"));
		
		model = new VenderItemModel();
	}
	
	@FXML protected boolean adicionar() {
		try {
			model.adicionar(nota, tableItens.getSelectionModel().getSelectedItem(), Double.parseDouble(textQuantidadeAdd.getText()));
			tableCarrinho.refresh();
		} catch(IllegalArgumentException e) {
			JOptionPane.showMessageDialog(null, "Entrada inv�lida.");
			return false;
		}
		return true;
	}
	
	@FXML protected void trocar() {
		if (remover()) {
			adicionar();
		}
	}
	
	@FXML protected boolean remover() {
		try {
			model.remover(nota, tableCarrinho.getSelectionModel().getSelectedItem(), Double.parseDouble(textQuantidadeRem.getText()));
			tableCarrinho.refresh();
		} catch(IllegalArgumentException e) {
			JOptionPane.showMessageDialog(null, "Entrada inv�lida.");
			return false;
		}
		return true;
	}
	
	@FXML protected void vender() {
		if (!textNota.isVisible()) {
			nota.processar();
			textNota.setVisible(true);
			textNota.setText(nota.toString());
		}
	}
	
	@FXML protected void voltar() {
		Main.telaPrincipal();
	}
}
