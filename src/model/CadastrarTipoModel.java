package model;

import main.Database;
import main.StrategyVenda;
import main.Tipo;

public class CadastrarTipoModel {
	public void adicionar(String nome, String codigo, StrategyVenda venda) throws Exception {
		Tipo tipo = new Tipo();
		tipo.setCodigo(codigo);
		tipo.setNome(nome);
		tipo.setVenda(venda);
		Database.getInstance().getTipos().add(tipo);
	}

	public void atualizar(Tipo tipo, String nome, String codigo, StrategyVenda venda) throws Exception {
		tipo.setCodigo(codigo);
		tipo.setNome(nome);
		tipo.setVenda(venda);
		Database.getInstance().exportar();
	}

	public void remover(Tipo tipo) {
		Database.getInstance().getTipos().remove(tipo);
	}
}
