package main;

public class Item implements java.io.Serializable {
	private static final long serialVersionUID = 317844492693306730L;
	private String marca;
	private String codigo;
	private double estoque;
	private double preco;
	private Tipo tipo;

	public String getMarca() {
		return marca;
	}
	
	public void setMarca(String marca) throws IllegalArgumentException {
		if (marca.isEmpty())
			throw new IllegalArgumentException();
		this.marca = marca;
	}
	
	public String getCodigo() {
		return codigo;
	}
	
	public void setCodigo(String codigo) throws IllegalArgumentException {
		if (codigo.isEmpty())
			throw new IllegalArgumentException();
		this.codigo = codigo;
	}
	
	public double getEstoque() {
		return estoque;
	}

	public String getEstoqueString() {
		return String.format(tipo.getVenda().getQuantidadeFormat(), estoque);
	}
	
	public void setEstoque(double estoque) {
		this.estoque = (estoque > 0) ? estoque : 0;
	}
	
	public double getPreco() {
		return preco;
	}
	
	public double getPrecoCompra() {
		return (3.0/4.0)*preco;
	}
	
	public String getPrecoString() {
		return String.format("%.02f", preco);
	}
	
	public String getPrecoStringExt() {
		return "R$ " + getPrecoString() + "/" + tipo.getVenda().getAbreviacao();
	}
	
	public String getPrecoCompraString() {
		return  "R$ " + String.format("%.02f", getPrecoCompra()) + "/" + tipo.getVenda().getAbreviacao();
	}
	
	public void setPreco(double preco) {
		this.preco = (preco > 0) ? preco : 0;
	}
	
	public Tipo getTipo() {
		return tipo;
	}
	
	public void setTipo(Tipo tipo) throws IllegalArgumentException {
		if (tipo == null)
			throw new IllegalArgumentException();
		this.tipo = tipo;
	}
	
	@Override
	public String toString() {
		return tipo.getNome() + " " + marca;
	}
}
