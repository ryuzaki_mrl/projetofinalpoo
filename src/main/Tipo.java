package main;

public class Tipo implements java.io.Serializable {
	private static final long serialVersionUID = 8200144835403824292L;
	private String codigo;
	private String nome;
	private StrategyVenda venda;

	public String getCodigo() {
		return codigo;
	}
	
	public void setCodigo(String codigo) throws IllegalArgumentException {
		if (codigo.isEmpty())
			throw new IllegalArgumentException();
		this.codigo = codigo;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) throws IllegalArgumentException {
		if (nome.isEmpty())
			throw new IllegalArgumentException();
		this.nome = nome;
	}
	
	public StrategyVenda getVenda() {
		return venda;
	}
	
	public void setVenda(StrategyVenda venda) throws IllegalArgumentException {
		if (venda == null)
			throw new IllegalArgumentException();
		this.venda = venda;
	}
	
	@Override
	public String toString() {
		return codigo + " " + nome;
	}
}
