package model;

import main.Item;
import main.ItemNota;
import main.NotaCompras;

public class VenderItemModel {
	public void adicionar(NotaCompras nota, Item item, double quantidade) throws IllegalArgumentException {
		if (nota == null || !nota.add(item, quantidade)) {
			throw new IllegalArgumentException();
		}
	}

	public void remover(NotaCompras nota, ItemNota item, double quantidade) throws IllegalArgumentException {
		if (nota == null || !nota.remover(item, quantidade)) {
			throw new IllegalArgumentException();
		}
	}
}
